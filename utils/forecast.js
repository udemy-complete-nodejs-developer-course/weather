const request = require('request')

const forecast = (latitude, longitude, callback) => {
    const url = 'https://api.darksky.net/forecast/cc6b292ff02f906700a3e05404c69f5f/' + latitude + ',' + longitude
    
    request({url: url, json: true}, (error, response) => {
        if(error){
            callback('Unable to connect to weather service', undefined)
        } else if(response.body.error) {
            callback('Unable to find location', undefined)
        } else {
            callback(undefined, {
                currentTemp:  response.body.currently.temperature,
                rainProb: response.body.currently.precipProbability,
                dailySummary: response.body.daily.data[0].summary
            })
        } 
    })
}

module.exports = forecast